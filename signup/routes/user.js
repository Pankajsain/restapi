const express = require('express');
const router = express.Router();
const User = require("../model/user");

// router.get('/', (req,res,next)=>{
//     res.status(200).json({
//         message: 'user get request'
//     })
// })

// router.post("/", (req,res,next) =>{
//     console.log(req.body);
//     const user = new User(req.body);
//     user.save().then(() =>{
//         res.status(201).send(user);
//     }).catch((e) =>{
//         res.status(400).send(e);
//     })
// })

// async await function
router.post("/", async(req,res) =>{
    try{
        const user = new User(req.body);
        const createUser= await user.save();
        res.status(201).send(createUser);
    }catch(e){
        res.status(400).send(e);
    }
})

router.get("/", async(req,res) =>{
    try{
        const usersData = await User.find();
        res.send(usersData);
    }catch(e){
        res.send(e);
    }
})

router.delete("/:id", async(req,res) =>{
    try{
        const _id = req.params.id;
        const removeUser = await User.deleteOne({_id});
        if(!removeUser){
            return res.status(404).send();
        }else{
            res.send(removeUser);
        }
        res.send(removeUser);
    }catch(e){
        res.send(e);
    }
})

router.put("/:id", async(req,res) =>{
    try{
        const _id = req.params.id;
        const userData = await User.updateOne({_id});

        if(!userData){
            return res.status(404).send();
        }else{
            res.send(userData);
        }
        res.send(userData);
    }catch(e){
        res.send(e);
    }
})

// single user data

router.get("/:id", async(req,res) =>{
    try{
        const _id = req.params.id;
        const userData = await User.findById({_id});

        if(!userData){
            return res.status(404).send();
        }else{
            res.send(usreData);
        }
        res.send(userData);
    }catch(e){
        res.send(e);
    }
})
module.exports = router;
 