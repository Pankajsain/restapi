const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require('bcrypt');

const userSchema = new mongoose.Schema({
    name:{
        type: String,
        required:true,
        minlenght:3   
    },
    email:{
        type:String,
        required:true,
        unique:[true, "Email is aready exists"],
        validate(value){
            if(!validator.isEmail(value)){
                throw new Error('Invlaid Email')
            }
        }
    },
    password: {
        type: String,
        required: true,
        bcrypt: true
    },
    phone:{
        type:Number,
        min:10,
        required:true,
        unique:true
    },
    address:{
        type:String,
        required: true,

    }
})


userSchema.pre('save', async function(next) {
    if(this.isModified("password")){
        this.password = await bcrypt.hash(this.password, 10);
    }
    next();
  })

const User = new mongoose.model('User', userSchema);

module.exports = User;