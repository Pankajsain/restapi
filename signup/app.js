const express = require('express');
const app = express();
const userRoute = require('./routes/user');
const bodyParser = require('body-parser');
const path = require('path');


require("./routes/db/conn");

const static_path = path.join(__dirname, "../public/index.html");
app.use(express.static(static_path));

app.get("/", (req, res) =>{
    res.send("root");
})

app.use(bodyParser.urlencoded({extended:false}));
app.use(bodyParser.json());

app.use('/users', userRoute);



app.use((req,res,next)=>{
    res.status(404).json({
        error: 'bad request'
    })
})

module.exports = app;