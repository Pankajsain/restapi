const express = require('express');
const app = express();
require('./db/conn');
const Register = require('./models/registers');
// const bodyParser = require('body-parser');


const path = require('path');
const hbs = require("hbs");
const port = process.env.PORT || 8000;


const static_path = path.join(__dirname, "../public");
const templates_path = path.join(__dirname, "../templates/views");
const partials_path = path.join(__dirname, "../templates/partials");

// const urlEncodedParser = app.use(bodyParser.urlencoded({extended:false}));
// app.use(bodyParser.json());


// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json()); // support json encoded bodies

app.use(express.json());
app.use(express.urlencoded({extended:false}));

// app.use(express.urlencoded({extended:false}));
//    // support parsing of application/json type post data
//    app.use(express.json());


app.use(express.static(static_path));
app.set("view engine", "hbs");
app.set("views", templates_path);
hbs.registerPartials(partials_path);

app.get("/", async(req,res) =>{
    res.render("index");
})

app.get("/register", async(req,res) =>{
    res.render("register");
})

app.post("/register", async(req,res) =>{
    try{
        console.log(req.body.firstname);
        // const user = new Student(req.body);
        // const createUser= await user.save();
        // res.status(201).send(createUser);
    }catch(e){
        res.status(400).send(e);
    }
})

app.listen(port, () => {
    console.log(`connected ${port}`);
})